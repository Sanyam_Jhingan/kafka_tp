# Apache kafka

Apache kafka is an Open source distributed streaming platform that allows for the development of real-time event driven applications. Kafka runs as a cluster that can span multiple servers or even multiple datacenters. It is known to process data records fast, in order and with high accuracy.

## **Terminology**
The terms given below are crucial for understanding kafka :

- **Producer**

  An application that sends data or messages to kafka.
- **Consumer**
  
  An application that reads data from kafka.
- **Broker**
  
  The kafka server that acts as a broker between producer and consumer.
- **Cluster**
  
  A group of computers sharing workload for a common purpose. So, a kafka cluster is a group of computers each executing one instance of kafka broker.
- **Topic**
  
  A topic is a unique name given to a kafka stream or dataset. It is an ordered list of events.
- **Partition**

  Partitioning is dividing a large Topic into multiple partitions to be stored on multiple computers. The user has to decide how many partitions a topic is divided into. 
- **Offset**
  
  A sequence ID given to messages as they arrive in a partition. Once assigned they can never change that is, they are immutable.
- **Consumer groups**
  
  A group of consumers acting as a single logical unit.

## **APIs**
Kafka is built on five core APIs : 

## 1. Producer API
The Producer API allows your application to produce to make the streams of data. So, it creates the records and produces them to topics. To use the producer, you can use the following maven dependency:
```
<dependency>
	<groupId>org.apache.kafka</groupId>
	<artifactId>kafka-clients</artifactId>
	<version>3.1.0</version>
</dependency>
```
## 2. Consumer API
The Consumer API subscribes to one or more topic, listens and ingests that data. It can subscribe to the topics on real time, or it can consume those old data records that are saved to the topic. To use the consumer, you can use the following maven dependency: 
```
<dependency>
	<groupId>org.apache.kafka</groupId>
	<artifactId>kafka-clients</artifactId>
	<version>3.1.0</version>
</dependency>
```
## 3. Streams API
The producers can produce directly to the consumers and that works for a simple kafka application where the data does not change. But to transform that data we need the Streams API. It leverages the producer and consumer APIs to consume from a topic or topics, and then it will analyze, agreggate and transform data in real time to produce the resulting stream to the same or new topics. To use Kafka Streams you can use the following maven dependency: 
```
<dependency>
	<groupId>org.apache.kafka</groupId>
	<artifactId>kafka-streams</artifactId>
	<version>3.1.0</version>
</dependency>
```
## 4. Connect API
The Connect API enables developers to write connectors which are reusable producers and consumers. In a kafka cluster many developers might need to integrate same types of data source. The Connect API provides developers with the intergration and the developers only need to configure it in order to get their data source into the cluster.

## 5. Admin API
The Admin API is the administrative client for kafka, which supports managing and inspecting topics, brokers, configs and other kafka objects. To use the Admin API, add the following Maven dependency:
```
<dependency>
	<groupId>org.apache.kafka</groupId>
	<artifactId>kafka-clients</artifactId>
	<version>3.1.0</version>
</dependency>
```
## **References**

- [Official Apache Kafka documentation](https://kafka.apache.org/documentation.html#introduction)
- [Apache Kafka tutorial Edureka](https://www.youtube.com/watch?v=hyJZP-rgooc)
- [Apache Kafka Crash Course](https://www.youtube.com/watch?v=R873BlNVUB4)
  
